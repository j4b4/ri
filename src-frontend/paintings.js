$(() => {
    updateButtonCount()
    const button = $('#nextPaintings');
    button.click(() => {
        showNextPaintings();
        updateButtonCount();
    })
});

function updateButtonCount() {
    const hiddenPaintings = $('.painting-box:not(:visible)');
    const button = $('#nextPaintings');
    const buttonCount = $('#nextPaintingsCount');
    if (hiddenPaintings.length > 0){
        buttonCount.html(hiddenPaintings.length);
    }else{
        button.hide();
    }
}
function showNextPaintings() {
    const hiddenPaintings = $('.painting-box:not(:visible):lt(10)');
    hiddenPaintings.slideDown()
}
