import $ from 'jquery';

window.$ = $;
$('.scroll-a').click((e) => {
    const href = $(e.currentTarget).attr('href');
    $('html, body').animate({
        scrollTop: ($(href).offset().top),
    }, 800);
});
