<?php declare(strict_types=1);

namespace App\Command;

use App\Entity\Image;
use App\EventListener\UrlCreator;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateUrlCommand extends Command {

    protected static $defaultName = 'app:script:generate-url';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(Connection $connection, EntityManagerInterface $entityManager) {
        parent::__construct(self::$defaultName);
        $this->connection = $connection;
        $this->entityManager = $entityManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $sql = "SELECT id FROM images WHERE url = ''";
        $ids = $this->connection->fetchAll($sql);

        foreach ($ids as $id) {
            $image = $this->entityManager->find(Image::class, $id['id']);
            $image->setUrl(UrlCreator::slugify($image->getName()));
            $this->entityManager->persist($image);
        }
        $this->entityManager->flush();
    }
}
