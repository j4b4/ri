<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    /**
     * @Route("/o-mne", name="about")
     */
    public function index()
    {
        return $this->render('about/index.html.twig', [
            'controllerName' => 'O mně',
        ]);
    }
}
