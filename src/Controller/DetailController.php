<?php
/**
 * Created by PhpStorm.
 * User: janbadura
 * Date: 09/05/2019
 * Time: 22:19
 */

namespace App\Controller;


use App\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DetailController extends AbstractController
{
    /**
     * @Route("/obraz/{url}", name="detail")
     */
    public function index($url)
    {
        if (!$url) {
            $this->redirectToRoute('homepage');
        }

        $repo = $this->getDoctrine()->getRepository(Image::class);
        /**@var Image $image*/
        $image = $repo->findOneBy(['url' => $url]);

        $remainingPaintings = count($repo->findAll()) - 1;
        $dimensions = $image->getSize();
        $material = $image->getMaterial();
        $technique = $image->getTechnique();
        $price = $image->getPrice();

        return $this->render('detail/index.html.twig', [
            'controllerName' => 'Detail obrazu',
            'remainingPaintings' => $remainingPaintings,
            'dimensions' => $dimensions,
            'material' => $material,
            'technique' => $technique,
            'price' => $price,
            'paint' => $image
        ]);
    }
}
